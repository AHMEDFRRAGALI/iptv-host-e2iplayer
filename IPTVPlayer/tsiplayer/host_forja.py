# -*- coding: utf8 -*-
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG
from Plugins.Extensions.IPTVPlayer.tsiplayer.libs.tstools import TSCBaseHostClass,tscolor
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta
from Components.config import config
import re
import base64,urllib
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads

def getinfo():
    info_={}
    info_['name']='Forja.Tn'
    info_['version']='1.0 14/07/2020'
    info_['dev']='RGYSoft'
    info_['cat_id']='41'
    info_['desc']='Movies & Series'
    info_['icon']='https://i.ibb.co/jVGTcfY/logo-Header.png'
    info_['recherche_all']='1'
    return info_


class TSIPHost(TSCBaseHostClass):
    def __init__(self):
        TSCBaseHostClass.__init__(self,{'cookie':'cimanow.cookie'})
        self.USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0'
        self.MAIN_URL = 'https://forja.tn'
        self.HEADER = {'User-Agent': self.USER_AGENT, 'Connection': 'keep-alive', 'Accept-Encoding':'gzip', 'Content-Type':'application/x-www-form-urlencoded','Referer':self.getMainUrl(), 'Origin':self.getMainUrl()}
        self.defaultParams = {'header':self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}
        self.getPage = self.cm.getPage
        
    def showmenu(self,cItem):
        self.addDir({'import':cItem['import'],'category' : 'host2','title':'Home','icon':cItem['icon'],'mode':'10','sub_mode':0,'url':self.MAIN_URL+'/home'})
        self.addDir({'import':cItem['import'],'category' : 'host2','title':'Movies','icon':cItem['icon'],'mode':'10','sub_mode':0,'url':self.MAIN_URL+'/movies'})
        self.addDir({'import':cItem['import'],'category' : 'host2','title':'Series','icon':cItem['icon'],'mode':'10','sub_mode':0,'url':self.MAIN_URL+'/series'})	
        #self.addDir({'import':cItem['import'],'category' : 'host2','title':'Ramadan','icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/ramadan'})		
        self.addDir({'import':cItem['import'],'category' :'search','title': _('Search'),'search_item':True,'page':1,'hst':'tshost','icon':'https://i.ibb.co/sqkBMBP/search.png'})

    def showmenu1(self,cItem):
        if '/home' in cItem['url']:
            cat=['movies']
            for elm_t in cat:
                #self.addMarker({'category' : 'host2','title':tscolor('\c00????00')+elm_t.title()})
                self.addDir({'import':cItem['import'],'category' : 'host2','title':'New '+elm_t.title(),'icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/api/'+elm_t+'/latest?extended=short&limit=30'})
                #self.addDir({'import':cItem['import'],'category' : 'host2','title':'New Episodes','icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/api/series/latest-episodes?extended=short&limit=30'})
                self.addDir({'import':cItem['import'],'category' : 'host2','title':'Most Watched ['+tscolor('\c00????00')+'Today'+tscolor('\c00??????') +']','icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/api/'+elm_t+'/most-watched?time=1&limit=30'})
                self.addDir({'import':cItem['import'],'category' : 'host2','title':'Most Watched ['+tscolor('\c00????00')+'This Week'+tscolor('\c00??????') +']','icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/api/'+elm_t+'/most-watched?time=7&limit=30'})		
                self.addDir({'import':cItem['import'],'category' : 'host2','title':'Most Watched ['+tscolor('\c00????00')+'This Month'+tscolor('\c00??????') +']','icon':cItem['icon'],'mode':'20','sub_mode':0,'url':self.MAIN_URL+'/api/'+elm_t+'/most-watched?time=30&limit=30'})		
        else:
            if '/movies' in cItem['url']: url = self.MAIN_URL+'/api/movies?extended=short'
            if '/series' in cItem['url']: url = self.MAIN_URL+'/api/series'
            self.addDir({'import':cItem['import'],'category' : 'host2','title':'All','icon':cItem['icon'],'mode':'20','sub_mode':1,'url':url})
            self.addDir({'import':cItem['import'],'category' : 'host2','title':tscolor('\c00????00')+'Filter','icon':cItem['icon'],'mode':'11','url':cItem['url']})

    def showmenu2(self,cItem):
        url=cItem['url']
        count = cItem.get('count',0)
        if count == 0:
            sts, data = self.getPage(url)
            if sts:
                Lst_inf = re.findall('<select(.*?)</select>',data, re.S)
                if Lst_inf:
                    self.addMarker({'category' : 'host2','title':tscolor('\c00????00')+'Genre:'})
                    Lst_inf0 = re.findall('<option.*?value="(.*?)".*?>(.*?)<',Lst_inf[0], re.S)
                    for genre,titre in Lst_inf0:
                        self.addDir({'import':cItem['import'],'category' : 'host2','title':titre,'icon':cItem['icon'],'mode':'11','url':cItem['url'],'genre':genre,'count':1,'data':Lst_inf[1]})
        else:
            genre = cItem.get('genre','')
            data  = cItem.get('data','')
            self.addMarker({'category' : 'host2','title':tscolor('\c00????00')+'Sort:'})
            Lst_inf0 = re.findall('<option.*?value="(.*?)".*?>(.*?)<',data, re.S)
            for sort,titre in Lst_inf0:
                if '/movies' in url: url = self.MAIN_URL+'/api/movies?extended=short'
                if '/series' in url: url = self.MAIN_URL+'/api/series'
                self.addDir({'import':cItem['import'],'category' : 'host2','title':titre,'icon':cItem['icon'],'mode':'20','url':url,'genre':genre,'sort':sort,'sub_mode':1})
                    
    def showitms(self,cItem):
        gnr   = cItem.get('sub_mode',0)
        page  = cItem.get('page',1)
        genre = cItem.get('genre','')
        sort  = cItem.get('sort','added')
        url   = cItem.get('url',self.MAIN_URL)
        if gnr==0:
            sts, data = self.getPage(url)
        else:
            if '/series' in url: post_data = {'page':page, 'title':'', 'genre':genre,'sortby':sort, 'no_data':'true'}
            else: post_data = {'page':page, 'title':'', 'genre':genre,'sortby':sort} 
            sts, data = self.getPage(url,post_data=post_data)
        if sts:	
            i=0
            data = json_loads(data)
            printDBG('data='+str(data))
            if "'series':" in str(data): type_ = 'serie'
            else: type_ = 'movie'
            try:
                data0  = data.get('series',data.get('movies',[]))
            except:
                data0  = data
            for elm in data0:
                elm_  = elm.get('mediaData',elm)
                titre = elm_.get('Title','')
                image = elm_.get('Poster','')
                URL   = self.MAIN_URL+'/'+type_+'/'+elm_.get('imdbID','')+'/'
                
                
                rating = str(elm_.get('imdbRating',''))
                year   = str(elm_.get('Year',''))
                genre  = str(elm_.get('Genre',''))
                desc = ''
                if rating!='': desc = desc + tscolor('\c00????00') + 'Rate: ' + tscolor('\c00??????') + rating +' | '
                if year!=''  : desc = desc + tscolor('\c00????00') + 'Year: ' + tscolor('\c00??????') + year +' | '
                if genre!='' : desc = desc + tscolor('\c00????00') + 'Genre: ' + tscolor('\c00??????') + genre +' | '

                i=i+1
                self.addDir({'import':cItem['import'],'category' : 'host2','title':titre,'icon':image,'mode':'21','url':URL,'desc':desc,'hst':'tshost','EPG':True,'good_for_fav':True})				
            if (gnr != 0):
                if (i>10):
                    self.addDir({'import':cItem['import'],'category' : 'host2','title':'Next Page','icon':cItem['icon'],'mode':'20','url':cItem['url'],'page':page+1,'genre':cItem.get('genre',''),'sort':cItem.get('sort','added'),'sub_mode':cItem['sub_mode'],'hst':'tshost','EPG':True})				

    def showelms(self,cItem):
        url = cItem['url']
        sts, data = self.getPage(url)
        if sts:
            printDBG('Data='+data)
            if '<div id="episodes">' in data:
                Lst_data0 = re.findall('episode-container">(.*?)overlay">(.*?)<',data, re.S)
                if Lst_data0:
                    i=0
                    Lst_data1 = re.findall('episodes =.*?\[(.*?)];',data, re.S)
                    if Lst_data1:
                        Lst_url = re.findall('sources:.*?\[(.*?)].*?poster.*?\'(.*?)\'.*?textTracks.*?\[(.*?)]',Lst_data1[0], re.S)
                        for elm in Lst_url:
                            URLS  = elm[0]
                            image = elm[1]
                            SUBS  = elm[2]
                            titre = Lst_data0[i][1]
                            Liste_els = re.findall('"language".*?["\'](.*?)["\'].*?"src".*?["\'](.*?)["\']', SUBS, re.S)
                            desc = cItem['desc'] + '\n' + tscolor('\c00????00')+'SUBS: '+tscolor('\c00??????')
                            for (tag,url) in Liste_els:						
                                if desc == cItem['desc'] + '\n' + tscolor('\c00????00')+'SUBS: '+tscolor('\c00??????'):
                                    desc= desc + tag.upper()
                                else:
                                    desc= desc + ' | ' + tag.upper()
                            
                            self.addVideo({'import':cItem['import'],'category' : 'host2','title':titre ,'url':URLS ,'desc':desc,'subs':SUBS,'icon':image,'hst':'tshost','good_for_fav':True})
                            i=i+1
            else:
                SUBS = ''
                Trailer =''
                Lst_data0 = re.findall('<video id="player"(.*?)</video>',data, re.S)
                if Lst_data0:
                    Lst_data0 = re.findall('<track(.*?)>',Lst_data0[0], re.S)
                    for sub_ in Lst_data0:
                        SUBS = SUBS+sub_
                
                Lst_data_t = re.findall('src="(https://www.youtube.com/embed/.*?)"',data, re.S)
                if Lst_data_t:
                    Trailer = Lst_data_t[0]
                
                Lst_data0 = re.findall('var streamSources.*?\[(.*?)\]',data, re.S)
                if Lst_data0:
                    URLS = Lst_data0[0]
                    Liste_els = re.findall('src="(.*?)".*?srclang="(.*?)"', SUBS, re.S)
                    desc = cItem['desc'] + '\n' + tscolor('\c00????00')+'SUBS: '+tscolor('\c00??????')
                    for (url,tag) in Liste_els:						
                        if desc == cItem['desc'] + '\n' + tscolor('\c00????00')+'SUBS: '+tscolor('\c00??????'):
                            desc= desc + tag.upper()
                        else:
                            desc= desc + ' | ' + tag.upper()
                    if Trailer!='':
                        self.addVideo({'import':cItem['import'],'category' : 'host2','title':'Trailer' ,'url':Trailer ,'desc':desc,'icon':cItem['icon'],'hst':'none'})
                    self.addVideo({'import':cItem['import'],'category' : 'host2','title':cItem['title'] ,'url':URLS ,'desc':desc,'subs':SUBS,'icon':cItem['icon'],'hst':'tshost','good_for_fav':True})
                            
    def SearchResult(self,str_ch,page,extra):
        
        self.addMarker({'title': tscolor('\c00????30') + 'Series:'})
        url = self.MAIN_URL+'/api/series'
        post_data = {'page':page,'title':str_ch,'genre':'','sortby':'updatedTime','no_data':'true'}
        sts, data = self.getPage(url,post_data=post_data)
        if sts:
            data = json_loads(data)
            printDBG('data='+str(data))
            if "'series':" in str(data): type_ = 'serie'
            else: type_ = 'movie'
            try:
                data0  = data.get('series',data.get('movies',[]))
            except:
                data0  = data
            for elm in data0:
                elm_  = elm.get('mediaData',elm)
                titre = elm_.get('Title','')
                image = elm_.get('Poster','')
                URL   = self.MAIN_URL+'/'+type_+'/'+elm_.get('imdbID','')+'/'
                rating = str(elm_.get('imdbRating',''))
                year   = str(elm_.get('Year',''))
                genre  = str(elm_.get('Genre',''))
                desc = ''
                if rating!='': desc = desc + tscolor('\c00????00') + 'Rate: ' + tscolor('\c00??????') + rating +' | '
                if year!=''  : desc = desc + tscolor('\c00????00') + 'Year: ' + tscolor('\c00??????') + year +' | '
                if genre!='' : desc = desc + tscolor('\c00????00') + 'Genre: ' + tscolor('\c00??????') + genre +' | '
                self.addDir({'import':extra,'category' : 'host2','title':titre,'icon':image,'mode':'21','url':URL,'desc':desc,'hst':'tshost','EPG':True})				


        self.addMarker({'title': tscolor('\c00????30') + 'Movies:'})
        url = self.MAIN_URL+'/api/movies?extended=short'
        post_data = {'page':page,'title':str_ch,'genre':'','sortby':'updatedTime','no_data':'true'}
        sts, data = self.getPage(url,post_data=post_data)
        if sts:
            data = json_loads(data)
            printDBG('data='+str(data))
            if "'series':" in str(data): type_ = 'serie'
            else: type_ = 'movie'
            try:
                data0  = data.get('series',data.get('movies',[]))
            except:
                data0  = data
            for elm in data0:
                elm_  = elm.get('mediaData',elm)
                titre = elm_.get('Title','')
                image = elm_.get('Poster','')
                URL   = self.MAIN_URL+'/'+type_+'/'+elm_.get('imdbID','')+'/'
                rating = str(elm_.get('imdbRating',''))
                year   = str(elm_.get('Year',''))
                genre  = str(elm_.get('Genre',''))
                desc = ''
                if rating!='': desc = desc + tscolor('\c00????00') + 'Rate: ' + tscolor('\c00??????') + rating +' | '
                if year!=''  : desc = desc + tscolor('\c00????00') + 'Year: ' + tscolor('\c00??????') + year +' | '
                if genre!='' : desc = desc + tscolor('\c00????00') + 'Genre: ' + tscolor('\c00??????') + genre +' | '
                self.addDir({'import':extra,'category' : 'host2','title':titre,'icon':image,'mode':'21','url':URL,'desc':desc,'hst':'tshost','EPG':True})				


    def getArticle(self,cItem):
        Desc = [('Runtime','<strong>Runtime:</strong.*?>(.*?)<','',''),('Genre','<strong>Genre:</strong.*?>(.*?)<','',''),('Language','<strong>Language:</strong.*?>(.*?)<','',''),('Story','<p>(.*?)</p>','\n','')]
        desc = self.add_menu(cItem,'','row desc">(.*?)class="row">','','desc',Desc=Desc)	
        if desc =='': desc = cItem.get('desc','')
        return [{'title':cItem['title'], 'text': desc, 'images':[{'title':'', 'url':cItem.get('icon','')}], 'other_info':{}}]


    def get_links(self,cItem): 	
        urlTab =[]
        SUBS = cItem['subs']
        URLS = cItem['url']
        subTrack = []
        if SUBS !='':
            Liste_els = re.findall('src="(.*?)".*?srclang="(.*?)"', SUBS, re.S)
            if Liste_els:
                for (url,tag) in Liste_els:
                    if url.startswith('/'): url = self.MAIN_URL + url
                    subTrack.append({'title':tag.upper(), 'url':url, 'lang':tag, 'format':'vtt'})
            else:
                Liste_els = re.findall('"language".*?["\'](.*?)["\'].*?"src".*?["\'](.*?)["\']', SUBS, re.S)
                for (tag,url) in Liste_els:
                    if url.startswith('/'): url = self.MAIN_URL + url
                    subTrack.append({'title':tag.upper(), 'url':url, 'lang':tag, 'format':'vtt'})			
        printDBG('Subs='+str(subTrack))
    
        Liste_els = re.findall('"src".*?["\'](.*?)["\'].*?"label".*?["\'](.*?)["\']', URLS, re.S)
        if not Liste_els:
            Liste_els = re.findall('src.*?["\'](.*?)["\'].*?label.*?["\'](.*?)["\']', URLS, re.S)
        for (url,tag) in Liste_els:
            if url.startswith('/'): url = self.MAIN_URL + url
            url = url.replace(' ','%20')
            URL=strwithmeta(url,{'external_sub_tracks':subTrack,'Referer':'https://forja.tn/','Origin':'https://forja.tn'})	
            urlTab.append({'name':'|'+tag+'| Local', 'url':URL, 'need_resolve':0, 'type':'local'})

        return urlTab	
            
    def getVideos(self,videoUrl):
        urlTab = []	
        code,id_ = videoUrl.split('|',1)
        if id_ == 'DOWN':
            sts, data = self.getPage(code,self.defaultParams)
            printDBG('data='+data)
            Liste_els = re.findall('id="downloadbtn".*?href="(.*?)"', data, re.S|re.IGNORECASE)			
            if Liste_els:
                url_ = Liste_els[0]
                if url_.endswith('mp4'):
                    host = url_.split('.net/',1)[0]+'.net'
                    URL_= strwithmeta('MP4|'+url_, {'Referer':host})
                    urlTab.append((URL_,'4'))	
                else:
                    urlTab.append((url_,'1'))
        else:	
            url = self.MAIN_URL+'/wp-content/themes/CimaNow/Interface/server.php'
            post_data = {'id':id_, 'server':code}
            sts, data = self.getPage(url,post_data=post_data)
            if sts:
                Liste_els_3 = re.findall('src="(.+?)"', data, re.S|re.IGNORECASE)	
                if Liste_els_3:
                    URL = Liste_els_3[0]
                    if URL.startswith('//'): URL='http:'+URL
                    if 'cimanow.net/' in URL:
                        host = URL.split('.net/',1)[0]+'.net'
                        printDBG('host='+host)
                        sts, data = self.getPage(URL,self.defaultParams)
                        printDBG('data='+data)
                        Liste_els = re.findall('source.*?src="(.*?)".*?size="(.*?)"', data, re.S|re.IGNORECASE)
                        for elm in Liste_els:
                            url_ = elm[0]
                            if not(url_.startswith('http')): url_ = host + urllib.quote(url_)
                            URL_= strwithmeta(elm[1]+'|'+url_, {'Referer':host})
                            urlTab.append((URL_,'4'))
                    else:
                        urlTab.append((URL,'1'))
        return urlTab			
                
    def start(self,cItem):
        mode=cItem.get('mode', None)
        if mode=='00':
            self.showmenu(cItem)
        elif mode=='10':
            self.showmenu1(cItem)	
        elif mode=='11':
            self.showmenu2(cItem)	
        elif mode=='20':
            self.showitms(cItem)
        elif mode=='21':
            self.showelms(cItem)		
        return True
